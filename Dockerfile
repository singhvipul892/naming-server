FROM openjdk:8-jdk-alpine
MAINTAINER vipcoder
WORKDIR /home/appuser
COPY target/naming-server-0.0.1-SNAPSHOT.jar naming-server-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["/usr/bin/java"]
CMD ["-jar", "naming-server-0.0.1-SNAPSHOT.jar"]
EXPOSE 9091